#pragma once

#include "shared/nulldelimiterbuilder.h"
#include <memory>
#include <string>

namespace px {
    class DynamicNullDelimiterBuilder {
    public:
        DynamicNullDelimiterBuilder();
        DynamicNullDelimiterBuilder(const DynamicNullDelimiterBuilder&) = delete;
        DynamicNullDelimiterBuilder(DynamicNullDelimiterBuilder&&) = delete;
        ~DynamicNullDelimiterBuilder();
        void add_token(const std::string& token);
        void add_token(const char* const token);
        int size();
        NullDelimiterBuilder build();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}