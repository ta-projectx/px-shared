#pragma once

#include <type_traits>

namespace px {
    namespace bitwiseenumclass {
        // credits: Anthony Williams and Andre Haupt (http://blog.bitwigglers.org)

        template<typename Enum>
        Enum operator|(Enum lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            using underlying = typename std::underlying_type<Enum>::type;
            return static_cast<Enum>(static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
        }

        template<typename Enum>
        Enum operator&(Enum lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            using underlying = typename std::underlying_type<Enum>::type;
            return static_cast<Enum>(static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
        }

        template<typename Enum>
        Enum operator^(Enum lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            using underlying = typename std::underlying_type<Enum>::type;
            return static_cast<Enum>(static_cast<underlying>(lhs) ^ static_cast<underlying>(rhs));
        }

        template<typename Enum>
        Enum operator|=(Enum& lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            return (lhs = lhs | rhs);
        }

        template<typename Enum>
        Enum operator&=(Enum& lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            return (lhs = lhs & rhs);
        }

        template<typename Enum>
        Enum operator^=(Enum& lhs, Enum rhs) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            return (lhs = lhs ^ rhs);
        }

        template<typename Enum>
        constexpr typename std::underlying_type<Enum>::type enumval(Enum ev) {
            static_assert(std::is_enum<Enum>::value, "template parameter is not an enum type");
            return static_cast<typename std::underlying_type<Enum>::type>(ev);
        }
    }
}