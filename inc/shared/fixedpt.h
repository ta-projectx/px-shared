/**
 * A fixed point arithmetic library.
 * Credits for Ivan Voras and Tim Hartrick for original implementation.
 */
#pragma once
#include <cstdint>

/*
 * fixedptc.h is a 32-bit or 64-bit fixed point numeric library.
 *
 * The symbol FIXPT_BITS, if defined before this library header file
 * is included, governs the number of bits in the data type (its "width").
 * The default width is 32-bit (FIXPT_BITS=32) and it can be used
 * on any recent C99 compiler. The 64-bit precision (FIXPT_BITS=64) is
 * available on compilers which implement 128-bit "long long" types. This
 * precision has been tested on GCC 4.2+.
 *
 * Since the precision in both cases is relatively low, many complex
 * functions (more complex than div & mul) take a large hit on the precision
 * of the end result because errors in precision accumulate.
 * This loss of precision can be lessened by increasing the number of
 * bits dedicated to the fraction part, but at the loss of range.
 *
 * Adventurous users might utilize this library to build two data types:
 * one which has the range, and one which has the precision, and carefully
 * convert between them (including adding two number of each type to produce
 * a simulated type with a larger range and precision).
 *
 * The ideas and algorithms have been cherry-picked from a large number
 * of previous implementations available on the Internet.
 * Tim Hartrick has contributed cleanup and 64-bit support patches.
 *
 * == Special notes for the 32-bit precision ==
 * Signed 32-bit fixed point numeric library for the 24.8 format.
 * The specific limits are -8388608.999... to 8388607.999... and the
 * most precise number is 0.00390625. In practice, you should not count
 * on working with numbers larger than a million or to the precision
 * of more than 2 decimal places. Make peace with the fact that PI
 * is 3.14 here. :)
 */

/*-
 * Copyright (c) 2010-2012 Ivan Voras <ivoras@freebsd.org>
 * Copyright (c) 2012 Tim Hartrick <tim@edgecast.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

typedef int32_t fixpt;
typedef int64_t fixptd;
typedef uint32_t fixptu;
typedef uint64_t fixptud;

union fixpt_union {
    fixpt fpt;
    struct {
        int16_t whole;
        int16_t fract;
    } part;
};
union fixptu_union {
    fixptu fpt;
    struct {
        uint16_t whole;
        int16_t fract;
    } part;
};

#define FIXPT_BITS 32
#define FIXPT_WBITS 16
#define FIXPT_FBITS (FIXPT_BITS - FIXPT_WBITS)
#define FIXPT_FMASK (((fixpt)1 << FIXPT_FBITS) - 1)

// add by this constant, then take out the whole part, to get the rounded number.
#define FIXPT_ROUND_BIT 0x8000

#define fixpt_rconst(R) ((fixpt)((R) * (((fixptd)1 << FIXPT_FBITS) + ((R) >= 0 ? 0.5 : -0.5))))
#define fixpt_fromint(I) ((fixpt)(I) << FIXPT_FBITS)
#define fixpt_toint(F) ((F) >> FIXPT_FBITS)
#define fixpt_xmul(A, B) (((fixptd)(A) * (fixptd)(B)) >> FIXPT_FBITS)
#define fixpt_xdiv(A, B) (((fixptd)(A) << FIXPT_FBITS) / (fixptd)(B))
#define fixpt_fracpart(A) ((fixpt)(A)&FIXPT_FMASK)
#define fixpt_round(A) (fixpt_toint((A + FIXPT_ROUND_BIT)))

#define FIXPT_ONE ((fixpt)((fixpt)1 << FIXPT_FBITS))

/* Multiplies two fixpt numbers, returns the result. */
static inline fixptd fixpt_mul(fixpt A, fixpt B) {
    return (((fixptd)A * (fixptd)B) >> FIXPT_FBITS);
}

/* Divides two fixpt numbers, returns the result. */
static inline fixptd fixpt_div(fixpt A, fixpt B) {
    return (((fixptd)A << FIXPT_FBITS) / (fixptd)B);
}

/*
 * Note: adding and substracting fixpt numbers can be done by using
 * the regular integer operators + and -.
 */

/**
 * Convert the given fixedpt number to a decimal string.
 * The max_dec argument specifies how many decimal digits to the right
 * of the decimal point to generate. If set to -1, the "default" number
 * of decimal digits will be used (2 for 32-bit fixedpt width, 10 for
 * 64-bit fixedpt width); If set to -2, "all" of the digits will
 * be returned, meaning there will be invalid, bogus digits outside the
 * specified precisions.
 */
static inline void fixpt_str(fixpt A, char* str, int max_dec) {
    int ndec = 0, slen = 0;
    char tmp[12] = {0};
    fixptud fr, ip;
    const fixptud one = (fixptud)1 << FIXPT_BITS;
    const fixptud mask = one - 1;

    if (max_dec == -1)
#if FIXPT_BITS == 32
        max_dec = 2;
#elif FIXPT_BITS == 64
        max_dec = 10;
#else
#error Invalid width
#endif
    else if (max_dec == -2)
        max_dec = 15;

    if (A < 0) {
        str[slen++] = '-';
        A *= -1;
    }

    ip = fixpt_toint(A);
    do {
        tmp[ndec++] = '0' + ip % 10;
        ip /= 10;
    } while (ip != 0);

    while (ndec > 0)
        str[slen++] = tmp[--ndec];
    str[slen++] = '.';

    fr = (fixpt_fracpart(A) << FIXPT_WBITS) & mask;
    do {
        fr = (fr & mask) * 10;

        str[slen++] = '0' + (fr >> FIXPT_BITS) % 10;
        ndec++;
    } while (fr != 0 && ndec < max_dec);

    if (ndec > 1 && str[slen - 1] == '0')
        str[slen - 1] = '\0'; /* cut off trailing 0 */
    else
        str[slen] = '\0';
}

/* Converts the given fixpt number into a string, using a static
 * (non-threadsafe) string buffer */
static inline char* fixpt_cstr(const fixpt A, const int max_dec) {
    static char str[25];

    fixpt_str(A, str, max_dec);
    return (str);
}