/**
 * Very basic array of array of char tokenizer.
 */
#pragma once

#include <memory>

namespace px {

    class ArrayToken {
    public:
        ArrayToken(int argc, char** argv);
        ArrayToken(ArrayToken&) = delete;
        ArrayToken(ArrayToken&&);
        ArrayToken& operator=(const ArrayToken&) = delete;
        ~ArrayToken();
        /**
         * Return current token and advance the tokenizer's position.
         * @return Pointer to token, or nullptr if tokenizer's position is past
         * the end of the array.
         */
        char* next_token();
        /**
         * Return current token without advancing the tokenizer's position.
         * @return Pointer to token, or nullptr if tokenizer's position is past
         * the end of the array.
         */
        char* peek_next_token();
        /**
         * To tell if current tokenizer's position is past the end of the array.
         * @return true if current tokenizer's position is past the end of the
         * array.
         */
        bool is_end();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}