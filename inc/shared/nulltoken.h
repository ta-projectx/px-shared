#pragma once

#include <memory>

namespace px {
    class NullToken {
    public:
        /**
         * @brief Construct a new NullToken, a simple null delimited string tokenizer.
         * @param data The char array buffer to tokenize.
         * @param len The expected length of the buffer.
         */
        NullToken(const char* const data, const int len);
        NullToken(NullToken&) = delete;
        NullToken& operator=(const NullToken&) = delete;
        ~NullToken();
        /**
         * Return current token and advance the tokenizer's position.
         * @return Pointer to token, or nullptr if tokenizer's position is past
         * the end of the data.
         */
        const char* next_token();
        /**
         * To tell if current tokenizer's position is past the end of the data.
         * @return true if current tokenizer's position is past the end of the
         * data.
         */
        bool is_end();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}