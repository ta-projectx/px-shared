#include "shared/compilerinfo.h"
#include <cstdio>

#define VERSTRR(x, y, z) #x "." #y "." #z
#define VERSTR(x, y, z) VERSTRR(x, y, z)

#ifdef __clang__
#define C_NAME "Clang"
#define C_VER VERSTR(__clang_major__, __clang_minor__, __clang_patchlevel__)
#define GCC_COMPATIBLE

#elif __GNUC__
#define C_NAME "GCC"
#define C_VER VERSTR(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)
#define GCC_COMPATIBLE

#else
#define C_NAME "(Unknown)"
#define C_VER "(Unknown)"
#endif

#define TO_BOOL(v) v ? 1 : 0

// various params
#ifdef GCC_COMPATIBLE

#ifdef NDEBUG
#define DEBUG_MODE 0
#else
#define DEBUG_MODE 1
#endif

#ifdef __OPTIMIZE__
#define GCC_OPTIMIZED 1
#else
#define GCC_OPTIMIZED 0
#endif

#endif

namespace px {
    namespace priv {
        const char* boolstr(bool b);
    }
}

const char* px::priv::boolstr(bool b) {
    static const char* y = "Yes";
    static const char* n = "No";
    return b ? y : n;
}

void px::print_compiler_info() {
    printf("Compiler: %s\n", C_NAME);
    printf("Version: %s\n", C_VER);
#ifdef GCC_COMPATIBLE
    printf("Debug Mode: %s\n", priv::boolstr(DEBUG_MODE));
    printf("Optimized: %s\n", priv::boolstr(GCC_OPTIMIZED));
#endif
}