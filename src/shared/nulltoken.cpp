#include "shared/nulltoken.h"
#include <cstdio>
#include <cstring>
#include <glog/logging.h>

using namespace px;
using namespace std;

class NullToken::Priv {
public:
    const char* const data;
    const int len;
    int pos;
    Priv(const char* const data, const int maxlen) : data{data}, len{maxlen}, pos{0} {}
    Priv(Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

NullToken::NullToken(const char* const data, const int len) {
    p = make_unique<Priv>(data, len);
}

NullToken::~NullToken() {}

const char* NullToken::next_token() {
    if (is_end()) {
        return nullptr;
    }
    const char* ret = &p->data[p->pos];
    p->pos += strlen(ret) + 1;

    // check if someone is attempting to do buffer overrun!
    if (p->pos > p->len) {
        LOG(WARNING) << "Buffer overrun on NullToken input";
        return nullptr;
    }

    return ret;
}

bool NullToken::is_end() {
    return p->pos >= p->len;
}