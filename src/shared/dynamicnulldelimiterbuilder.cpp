#include "shared/dynamicnulldelimiterbuilder.h"
#include <cstring>
#include <vector>

using namespace px;
using namespace std;

class DynamicNullDelimiterBuilder::Priv {
public:
    vector<string> data;
    int size_ctr = 0;
};

DynamicNullDelimiterBuilder::DynamicNullDelimiterBuilder() {
    p = make_unique<Priv>();
}

DynamicNullDelimiterBuilder::~DynamicNullDelimiterBuilder() {}

void DynamicNullDelimiterBuilder::add_token(const string& token) {
    p->data.push_back(token);
    p->size_ctr += token.size() + 1;
}

void DynamicNullDelimiterBuilder::add_token(const char* const token) {
    string s = token;
    add_token(s);
}

int DynamicNullDelimiterBuilder::size() {
    return p->size_ctr;
}

NullDelimiterBuilder DynamicNullDelimiterBuilder::build() {
    NullDelimiterBuilder ret(p->size_ctr);
    for (string d : p->data) {
        ret.add_token(d);
    }
    return ret;
}