#include "shared/util.h"
#include <cstdarg>
#include <cstdlib>

using namespace px;
using namespace std;

util::DeleteInvoker::DeleteInvoker(std::function<void(void)> fn) {
    this->fn = fn;
}

util::DeleteInvoker::~DeleteInvoker() {
    fn();
}

void util::fputsn(const char* const data, FILE* stream) {
    fputs(data, stream);
    fputc('\n', stream);
}

string util::string_printf(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);

    char* ret_ptr;
    int ret_ptr_len = vasprintf(&ret_ptr, fmt, args);

    // automatic deletor, no matter what
    DeleteInvoker di([ret_ptr_len, &args, &ret_ptr]() {
        va_end(args);
        if (ret_ptr_len >= 0) {
            free(ret_ptr);
        }
    });

    // return an error string on error
    return ret_ptr_len > 0 ? ret_ptr : "<error>";
}

string util::get_default_app_runtime_dir(string appname) {
    char* env_path = getenv("XDG_RUNTIME_DIR");
    string path = env_path == nullptr ? "/tmp/" : env_path;
    if (path.back() != '/') {
        path.append("/");
    }
    return path;
}

string util::get_default_lockfile_path(string appname) {
    string ret = get_default_app_runtime_dir(appname) + appname + ".lock";
    return ret;
}

string util::get_default_socketfile_path(string appname) {
    string ret = get_default_app_runtime_dir(appname) + appname + ".socket";
    return ret;
}

string util::get_default_pid_path(string appname) {
    string ret = get_default_app_runtime_dir(appname) + appname + ".pid";
    return ret;
}