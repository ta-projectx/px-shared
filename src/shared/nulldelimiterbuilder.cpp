#include "shared/nulldelimiterbuilder.h"
#include "shared/memory.h"
#include <cstring>

using namespace px;
using namespace px::memory;
using namespace std;

class NullDelimiterBuilder::Priv {
public:
    unique_ptr<char, free_delete> buff;
    int maxsize;
    int wpos = 0;

    Priv() {}
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;

    int add_token(const char* const token, int size);
};

NullDelimiterBuilder::NullDelimiterBuilder() {
    p = make_unique<Priv>();
    p->maxsize = 0;
}

NullDelimiterBuilder::NullDelimiterBuilder(int maxsize) {
    p = make_unique<Priv>();
    p->maxsize = maxsize;
    p->buff = make_unique_malloc<char>((char*)malloc(maxsize));
}

NullDelimiterBuilder::NullDelimiterBuilder(std::string sstr) : NullDelimiterBuilder(sstr.size() + 1) {
    add_token(sstr);
}

NullDelimiterBuilder::NullDelimiterBuilder(NullDelimiterBuilder&& src) {
    p = move(src.p);
}

NullDelimiterBuilder::~NullDelimiterBuilder() {}

NullDelimiterBuilder& NullDelimiterBuilder::operator=(NullDelimiterBuilder&& src) {
    p = move(src.p);
    return *this;
}

int NullDelimiterBuilder::add_token(const std::string& token) {
    return p->add_token(token.c_str(), token.size());
}

int NullDelimiterBuilder::add_token(const char* const token) {
    return p->add_token(token, strlen(token));
}

int NullDelimiterBuilder::Priv::add_token(const char* const token, int size) {
    if (size >= (maxsize - wpos)) {
        return 0;
    }
    memcpy(&buff.get()[wpos], token, size);
    wpos += size;
    buff.get()[wpos] = 0;
    ++wpos;
    return size;
}

int NullDelimiterBuilder::size() {
    return p->wpos;
}

const char* const NullDelimiterBuilder::get_buffer() {
    return p->buff.get();
}