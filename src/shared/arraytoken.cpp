#include "shared/arraytoken.h"

using namespace px;
using namespace std;

struct ArrayToken::Priv {
    int pos;
    int argc;
    char** argv;

    Priv() {}
    Priv(Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

ArrayToken::ArrayToken(int argc, char** argv) {
    p = make_unique<Priv>();
    p->argc = argc;
    p->argv = argv;
    p->pos = 0;
}

ArrayToken::ArrayToken(ArrayToken&& src) {
    p = move(src.p);
}

ArrayToken::~ArrayToken() {}

char* ArrayToken::next_token() {
    if (p->pos >= p->argc) {
        return nullptr;
    }
    return p->argv[p->pos++];
}

char* ArrayToken::peek_next_token() {
    if (p->pos >= p->argc) {
        return nullptr;
    }
    return p->argv[p->pos];
}

bool ArrayToken::is_end() {
    return p->pos >= p->argc;
}